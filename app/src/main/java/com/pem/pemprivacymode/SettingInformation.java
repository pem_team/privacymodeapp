package com.pem.pemprivacymode;

import java.util.ArrayList;

public class SettingInformation {
    private String key;
    private String title;
    private String iconName;
    private String description;
    private String valueString;
    private boolean valueBool;
    private boolean hasToggle;
    private ArrayList<String> list;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ArrayList<String> getList() {
        return list;
    }

    public void setList(ArrayList<String> list) {
        this.list = list;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValueString() {
        return valueString;
    }

    public void setValueString(String valueString) {
        this.valueString = valueString;
    }

    public boolean isValueBool() {
        return valueBool;
    }

    public void setValueBool(boolean valueBool) {
        this.valueBool = valueBool;
    }

    public boolean isHasToggle() {
        return hasToggle;
    }

    public void setHasToggle(boolean hasToggle) {
        this.hasToggle = hasToggle;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }
}
