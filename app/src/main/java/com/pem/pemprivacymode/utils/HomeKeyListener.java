package com.pem.pemprivacymode.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

public class HomeKeyListener {
    static final String TAG = "HomeKeyListener";
    private Context mContext;
    private IntentFilter mFilter;
    private OnHomePressedListener mListener;
    private InnerRecevier mRecevier;
    /**
     Making * Home key monitoring structure initialization
     * @param context
     */
    public HomeKeyListener(Context context) {
        mContext = context;
        mFilter = new IntentFilter();
        mFilter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        mFilter.addAction(Intent.ACTION_SCREEN_OFF);
        mFilter.addAction(Intent.ACTION_SCREEN_ON);

    }

    /**
     * callback interface
     * @author zhouchaoxin
     *
     */
    public interface OnHomePressedListener {

        /**
         Making * Home key press
         */
        public void onHomePressed();
        /**
         Making * Home key long press
         */
        public void onHomeLongPressed();
        /**
         Making * monitor the power key / open
         */
        public void onScreenPressed();
        /**
         Making * monitor the power button on / off
         */
        public void offScreenPressed();
    }

    /**
     Making * set monitor
     * @param listener
     */
    public void setOnHomePressedListener(OnHomePressedListener listener) {
        mListener = listener;
        mRecevier = new InnerRecevier();
    }

    /**
     * start listening, registered broadcasting
     */
    public void startHomeListener() {
        if (mRecevier != null) {
            mContext.registerReceiver(mRecevier, mFilter);
        }
    }

    /**
     * stop listening, cancellation of broadcasting
     */
    public void stopHomeListener() {
        if (mRecevier != null) {
            mContext.unregisterReceiver(mRecevier);
        }
    }
    /**
     Making * broadcast reception
     * @author zhouchaoxin
     *
     */
    class InnerRecevier extends BroadcastReceiver {
        final String SYSTEM_DIALOG_REASON_KEY = "reason";
        final String SYSTEM_DIALOG_REASON_GLOBAL_ACTIONS = "globalactions";
        final String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";
        final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {

                String reason = intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY);
                if (reason != null) {
                    Log.e(TAG, "action:" + action + ",reason:" + reason);
                    if (mListener != null) {
                        if (reason.equals(SYSTEM_DIALOG_REASON_HOME_KEY)) {
                            // Short press home key
                            mListener.onHomePressed();
                        } else if (reason.equals(SYSTEM_DIALOG_REASON_RECENT_APPS)) {
                            // Long press home
                            mListener.onHomeLongPressed();
                        }
                    }
                }
            }//Monitor the power button.
            if (action.equals(Intent.ACTION_SCREEN_OFF)) {

                mListener.offScreenPressed();

            }else if (action.equals(Intent.ACTION_SCREEN_ON)) {

                mListener.onScreenPressed();
            }
        }
    }
}