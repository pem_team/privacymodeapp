package com.pem.pemprivacymode.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pem.pemprivacymode.GlobalData;
import com.pem.pemprivacymode.PrivacyMode;
import com.pem.pemprivacymode.R;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class InstantSharingDialog extends Dialog {
    private ImageView dialogIcon;
    private ImageView appIcon;
    private int dialogIconResource;
    private Bitmap dialogIconBitmap;
    private TextView dialogTitle;
    private String dialogTitleText = "";
    private ListView dialogList;
    private ArrayAdapter adapter;
    private Activity activity;

    public DialogListOnClickListener dialogListListener;
    public ButtonOnClickListener dialogNegativeButtonListener;
    public ButtonOnClickListener dialogPositiveButtonListener;

    private TextView dialogMessage;
    private Button dialogNegativeButton;
    private Button dialogPositiveButton;

    private String dialogMessageText;
    private String dialogNegativeText;
    private String dialogPositiveText;

    private View dialogCustomView;
    private FrameLayout dialogCustom;

    private RelativeLayout dialogFreeze;
    private RelativeLayout dialogApp;
    private RelativeLayout dialogPrivacy;

    public InstantSharingDialog(Activity a) {
        super(a);
        this.activity = a;
    }

    public InstantSharingDialog(Context c) {
        super(c);
    }

    public interface DialogListOnClickListener {
        void onListClick(int which);
    }

    public interface ButtonOnClickListener {
        void onButtonClick();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        showListDialog();

        /*if(adapter != null) {
            showListDialog();
        } else if(dialogMessageText != null) {
            showMessageDialog();
        } else if(dialogCustomView != null) {
            showCustomDialog();
        }*/

        dialogFreeze = (RelativeLayout) findViewById(R.id.dialogFreeze);
        dialogApp = (RelativeLayout) findViewById(R.id.dialogApp);
        dialogPrivacy = (RelativeLayout) findViewById(R.id.dialogPrivacy);
        appIcon = (ImageView) findViewById(R.id.appIcon);

        try {
            if(activity != null) {
                String activityName = activity.getClass().getSimpleName();
                String internalName = GlobalData.getInstance().getNameForApplicationActivity(activityName);
                String search = GlobalData.getInstance().getPackageRegex(internalName);
                String packageName = getPackageNameForSearch(search);
                Drawable icon = getContext().getPackageManager().getApplicationIcon(packageName);
                appIcon.setImageDrawable(icon);
            }
        } catch (PackageManager.NameNotFoundException ne) {
        }

        dialogFreeze.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrivacyMode.getInstance().changeMode(PrivacyMode.ACCESS_INSTANT_FREEZE);
                dismiss();
            }
        });

        dialogApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrivacyMode.getInstance().changeMode(PrivacyMode.ACCESS_INSTANT_APP);
                dismiss();
            }
        });

        dialogPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrivacyMode.getInstance().changeMode(PrivacyMode.ACCESS_RESTRICTED);
                dismiss();
            }
        });
    }


    private void showListDialog() {
        setContentView(R.layout.dialog_list);

        /*dialogList = (ListView) findViewById(R.id.dialogList);
        dialogList.setAdapter(adapter);

        dialogList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogListListener.onListClick(position);
                dismiss();
            }
        });*/
    }

    /*private void showMessageDialog() {
        setContentView(R.layout.dialog_message);

        dialogMessage = (TextView) findViewById(R.id.dialogMessage);
        dialogMessage.setText(dialogMessageText);

        dialogNegativeButton = (Button) findViewById(R.id.dialogNegativeButton);
        dialogNegativeButton.setText(dialogNegativeText);
        dialogNegativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogNegativeButtonListener.onButtonClick();
            }
        });

        dialogPositiveButton = (Button) findViewById(R.id.dialogPositiveButton);
        dialogPositiveButton.setText(dialogPositiveText);
        dialogPositiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPositiveButtonListener.onButtonClick();
                dismiss();
            }
        });

    }

    private void showCustomDialog() {
        setContentView(R.layout.dialog_custom);

        dialogCustom = (FrameLayout) findViewById(R.id.dialogCustom);

        dialogCustom.addView(dialogCustomView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        dialogNegativeButton = (Button) findViewById(R.id.dialogNegativeButton);
        dialogNegativeButton.setText(dialogNegativeText);
        dialogNegativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogNegativeButtonListener.onButtonClick();
            }
        });

        dialogPositiveButton = (Button) findViewById(R.id.dialogPositiveButton);
        dialogPositiveButton.setText(dialogPositiveText);
        dialogPositiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPositiveButtonListener.onButtonClick();
                dismiss();
            }
        });
    }*/

    public void setTitle(String title) {
        dialogTitleText = title;
    }

    public void setList(ArrayAdapter adapter) {
        this.adapter = adapter;
    }

    public void setDialogListOnClickListener(DialogListOnClickListener listener) {
        this.dialogListListener = listener;
    }

    public void setIcon(int resource) {
        this.dialogIconResource = resource;
    }

    public void setIcon(Bitmap bitmap) {
        this.dialogIconBitmap = bitmap;
    }

    public void setMessage(String msg ) {
        this.dialogMessageText = msg;
    }

    public void setNegativeButton(String str, ButtonOnClickListener listener) {
        this.dialogNegativeText = str;
        this.dialogNegativeButtonListener = listener;
    }

    public void setPositiveButton(String str, ButtonOnClickListener listener) {
        this.dialogPositiveText = str;
        this.dialogPositiveButtonListener = listener;
    }

    public void setCustomView(View view) {
        this.dialogCustomView = view;
    }


    public String getPackageNameForSearch(String search) {
        if(activity == null) {
            return "";
        }
        List<ApplicationInfo> apps = activity.getApplicationContext().getPackageManager().getInstalledApplications(0);
        search = search.isEmpty() ? "" : ".*" + search + ".*";
        Pattern p = Pattern.compile(search);
        for (int i = 0; i < apps.size(); i++) {
            String pkg = apps.get(i).packageName;
            Matcher m = p.matcher(pkg);

            if (m.matches() && getContext().getPackageManager().getLaunchIntentForPackage(pkg) != null) {
                return pkg;
            }
        }
        return "";
    }

}
