package com.pem.pemprivacymode;

import android.support.annotation.Nullable;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class GlobalData{
    private static GlobalData instance;

    // Global variable
    private int data;
    private Map<String, String> applicationList; // The full list of applications
    private Map<String, String> applicationActivities; // The applications we have custom activities for
    private String[] applicationGrid = { // Names in the application grid depend on the key in applicationList
            "", "", "", "",
            "", "gallery", "camera", "",
            "", "", "", "settings",
            "", "", "", "",
            "youtube", "whatsapp", "messenger", "calendar",
            "dialer", "sms", "email", "browser"
    };
    private String[] hiddenApplicationsPrivate = {
            "messenger", "email"
    };


    // Restrict the constructor from being instantiated
    private GlobalData() {
        // Generate full application list (key = common name, value = regex for package name)
        applicationList = new HashMap<>();
        applicationList.put("gallery", "(gallery|photos)");
        applicationList.put("gallery2", "(gallery|photos)");
        applicationList.put("camera", "camera");
        applicationList.put("settings", "settings");
        applicationList.put("youtube", "youtube");
        applicationList.put("whatsapp", "whatsapp");
        applicationList.put("facebook", "facebook");
        applicationList.put("messenger", "facebook.orca");
        applicationList.put("calendar", "calendar");
        applicationList.put("dialer", "(phone|dialer)");
        applicationList.put("sms", "messaging");
        applicationList.put("email", "email");
        applicationList.put("browser", "(chrome|browser)");

        // Generate list with custom activity names
        applicationActivities = new HashMap<>();
        applicationActivities.put("gallery", "GalleryActivity2");
        applicationActivities.put("gallery2", "SwipeViewActivity");
        applicationActivities.put("whatsapp", "WhatsAppActivity");
        applicationActivities.put("browser", "ChromeActivity");
        applicationActivities.put("dialer", "DialerActivity");
        applicationActivities.put("settings", "SettingsActivity");

    }

    public void setData(int d){
        this.data=d;
    }
    public int getData(){
        return this.data;
    }

    public Map<String, String> getApplicationList() {
        return this.applicationList;
    }

    public String[] getApplicationGrid() {
        return this.applicationGrid;
    }

    public String getPackageRegex(String key) {
        if(key.isEmpty()) {
            return "";
        }
        return this.applicationList.get(key);
    }

    public String getApplicationActivity(String key) {
        if(this.applicationActivities.containsKey(key)) {
            return this.applicationActivities.get(key);
        }
        return "";
    }

    public String getNameForApplicationActivity(String value) {
        if(this.applicationActivities.containsValue(value)) {
            //return this.applicationActivities.
            return getKeyByValue(this.applicationActivities, value);
        }
        return "";
    }

    public String[] getHiddenApplicationsPrivate() {
        return this.hiddenApplicationsPrivate;
    }

    public boolean isApplicationHiddenPrivate(String key) {
        return Arrays.asList(this.hiddenApplicationsPrivate).contains(key);
    }

    public static synchronized GlobalData getInstance(){
        if(instance==null){
            instance=new GlobalData();
        }
        return instance;
    }

    private <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }
}
