package com.pem.pemprivacymode;

import android.util.Log;

public class PrivacyMode {
    public static final int ACCESS_FULL = 0;
    public static final int ACCESS_RESTRICTED = 1;
    public static final int ACCESS_GUEST = 2;
    public static final int ACCESS_INSTANT_FREEZE = 3;
    public static final int ACCESS_INSTANT_APP = 4;

    public interface OnPrivacyModeChangeListener {
        void privacyModeChanged(int newMode);
    }

    private static PrivacyMode mInstance;
    private OnPrivacyModeChangeListener mListener;
    private int mMode = -1;

    private PrivacyMode() {}

    public static PrivacyMode getInstance() {
        if(mInstance == null) {
            mInstance = new PrivacyMode();
        }
        return mInstance;
    }

    public void setListener(OnPrivacyModeChangeListener listener) {
        mListener = listener;
    }

    public void changeMode(int mode) {
        mMode = mode;
        if(mListener != null) {
            notifyModeChange(mMode);
        }
    }

    public int getMode() {
        return mMode;
    }

    private void notifyModeChange(int newMode) {
        mListener.privacyModeChanged(newMode);
    }
}
