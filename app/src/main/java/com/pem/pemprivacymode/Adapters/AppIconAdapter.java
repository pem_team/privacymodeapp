package com.pem.pemprivacymode.Adapters;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.List;

public class AppIconAdapter extends BaseAdapter {
    private Context mContext;

    public AppIconAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return packageNames.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            final float scale = mContext.getResources().getDisplayMetrics().density;
            int pixels = (int) (56 * scale + 0.5f);
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(pixels, pixels));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }

        try {
            Drawable icon = mContext.getPackageManager().getApplicationIcon(packageNames[position]);
            imageView.setImageDrawable(icon);
        } catch (PackageManager.NameNotFoundException e)
        {}

        /*
        // List installed package names
        final PackageManager pm = mContext.getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo packageInfo : packages) {
            Log.d("", "Installed package :" + packageInfo.packageName);
        }*/

        //imageView.setImageResource(mThumbIds[position]);

        imageView.setTag(packageNames[position]);

        return imageView;
    }

    // references to our images
    private String[] packageNames = {
            "", "", "", "",
            "com.google.android.youtube", "com.whatsapp", "com.facebook.orca", "com.android.calendar",
            "com.android.dialer", "com.android.messaging", "com.android.email", "com.android.chrome"
    };
}
