package com.pem.pemprivacymode.Adapters;


import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.pem.pemprivacymode.R;

public class NumberDialerAdapter extends BaseAdapter {
    private Context myContext;
    private int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};

    public NumberDialerAdapter(Context c) {myContext = c;}

    @Override
    public int getCount() {
        return numbers.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public ImageView getView(int position, View convertView, ViewGroup parent) {
        ImageView button;

        if(convertView==null) {
           final float scale = myContext.getResources().getDisplayMetrics().density;
           int pixels = (int) (56 * scale + 0.5f);
            // if it's not recycled, initialize some attributes
            button = new ImageView(myContext);
            button.setLayoutParams(new GridView.LayoutParams(pixels, pixels));
            button.setScaleType(ImageView.ScaleType.CENTER_CROP);
            button.setPadding(8, 8, 8, 8);
        } else {
            button = (ImageView) convertView;
        }


        String ident = "dialer_" + numbers[position];
        int id = myContext.getResources().getIdentifier(ident, "drawable", myContext.getPackageName());
        button.setImageResource(id);

        button.setTag(String.valueOf(numbers[position]));

        return button;
    }


}
