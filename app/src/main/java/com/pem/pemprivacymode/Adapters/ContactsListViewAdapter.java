package com.pem.pemprivacymode.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pem.pemprivacymode.R;


public class ContactsListViewAdapter extends BaseAdapter {
    String [] name;
    Context context;
    int [] profilePicture;

    private static LayoutInflater inflater=null;

    public ContactsListViewAdapter(Activity activity, int[] profilePictures, String[] names) {
        context=activity;
        name=names;
        profilePicture=profilePictures;

        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return name.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView textViewName;
        ImageView imageView;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.contact_row, null);

        holder.textViewName=(TextView) rowView.findViewById(R.id.textViewName2);
        holder.imageView=(ImageView) rowView.findViewById(R.id.profilePicture2);

        holder.textViewName.setText(name[position]);
        holder.imageView.setImageResource(profilePicture[position]);

        return rowView;
    }


}
