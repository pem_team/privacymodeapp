package com.pem.pemprivacymode.Adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.pem.pemprivacymode.Homescreen.AppModel;
import com.pem.pemprivacymode.R;

import java.util.ArrayList;
import java.util.Collection;

public class SettingsAppListAdapter extends ArrayAdapter<AppModel> {
    private final LayoutInflater mInflater;

    public SettingsAppListAdapter (Context context) {
        super(context, android.R.layout.simple_list_item_2);
        mInflater = LayoutInflater.from(context);
    }

    public void setData(ArrayList<AppModel> data) {
        clear();
        if (data != null) {
            addAll(data);
        }
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void addAll(Collection<? extends AppModel> items) {
        //If the platform supports it, use addAll, otherwise add in loop
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            super.addAll(items);
        }else{
            for(AppModel item: items){
                super.add(item);
            }
        }
    }

    /**
     * Populate new items in the list.
     */
    @Override public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.settings_app_item, parent, false);
        } else {
            view = convertView;
        }

        ImageView icon = (ImageView) view.findViewById(R.id.appListIcon);
        TextView text = (TextView) view.findViewById(R.id.appListName);
        Switch switch1 = (Switch) view.findViewById(R.id.appListSwitch);

        if(icon == null || text == null) {
            return view;
        }

        AppModel item = getItem(position);
        if(item != null) {
            icon.setImageDrawable(item.getIcon());
            text.setText(item.getLabel());
        } else {
            icon.setImageDrawable(null);
            text.setText(null);
        }

        return view;
    }
}
