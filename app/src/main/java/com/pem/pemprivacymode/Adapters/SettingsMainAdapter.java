package com.pem.pemprivacymode.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.mikepenz.iconics.view.IconicsImageView;
import com.pem.pemprivacymode.R;
import com.pem.pemprivacymode.SettingInformation;

import java.util.ArrayList;

public class SettingsMainAdapter extends ArrayAdapter<SettingInformation> {
    private Context context;

    public SettingsMainAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }

    public SettingsMainAdapter(Context context, int resource, ArrayList<SettingInformation> items) {
        super(context, resource, items);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.settings_main_item, null);
        }

        final SettingInformation data = getItem(position);
        String titleText = data.getTitle();
        String descriptionText = data.getDescription();
        String iconName = data.getIconName();

        IconicsImageView settingIcon = (IconicsImageView) v.findViewById(R.id.settingsIcon);
        if(iconName != null) {
            settingIcon.setIcon(iconName);
        } else {
            settingIcon.setIcon("");
        }

        TextView title = (TextView) v.findViewById(R.id.firstLine);
        title.setText(titleText);

        TextView secondLine = (TextView) v.findViewById(R.id.secondLine);
        secondLine.setText(descriptionText);

        if(descriptionText == null || descriptionText.isEmpty()) {
            secondLine.setVisibility(View.GONE);
        } else {
            secondLine.setVisibility(View.VISIBLE);
        }

        //ImageView albumArt = (ImageView) v.findViewById(R.id.albumArt);

        TextView active = (TextView) v.findViewById(R.id.activeText);

        final Switch switchView = (Switch) v.findViewById(R.id.switch1);
        switchView.setClickable(true);
        switchView.setFocusable(true);
        if(data.isHasToggle()) {
            switchView.setVisibility(View.VISIBLE);
            switchView.setChecked(data.isValueBool());
        } else {
            switchView.setVisibility(View.GONE);
        }

        /*if(data.isHasToggle()) {
            switchView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switchView.toggle();
                }
            });
        }*/

            /*v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switchView.toggle();
                }
            });*/


        v.setTag(data);

        return v;
    }

}
