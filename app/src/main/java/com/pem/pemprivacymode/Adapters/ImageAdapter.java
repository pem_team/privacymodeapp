package com.pem.pemprivacymode.Adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;

import com.pem.pemprivacymode.AppActivities.GalleryActivity2;
import com.pem.pemprivacymode.PrivacyMode;
import com.pem.pemprivacymode.R;


public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    GalleryActivity2 gallery;
    Integer [] mThumbs;

    public ImageAdapter(Context c, Integer[] mThumbsIds) {
        mContext = c;
        mThumbs = mThumbsIds;
    }

    public int getCount() {
        return mThumbs.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }



    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(300, 300));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(mThumbs[position]);
        return imageView;
    }

    // references to our images
    /*private Integer[] mThumbIds = {
            R.drawable.bed,
            R.drawable.urlaub1,
            R.drawable.urlaub2,
            R.drawable.urlaub3,
            R.drawable.urlaub4,
            R.drawable.beach
    };*/


}