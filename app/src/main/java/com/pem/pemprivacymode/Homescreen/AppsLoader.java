package com.pem.pemprivacymode.Homescreen;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.pem.pemprivacymode.GlobalData;
import com.pem.pemprivacymode.HomescreenActivity;
import com.pem.pemprivacymode.PrivacyMode;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @credit http://developer.android.com/reference/android/content/AsyncTaskLoader.html
 */
public class AppsLoader extends AsyncTaskLoader<ArrayList<AppModel>> {
    ArrayList<AppModel> mInstalledApps;

    final PackageManager mPm;
    PackageIntentReceiver mPackageObserver;

    private int privacyMode = -1;

    public AppsLoader(Context context) {
        super(context);

        privacyMode = PrivacyMode.getInstance().getMode();
        mPm = context.getPackageManager();
    }

    @Override
    public ArrayList<AppModel> loadInBackground() {
        privacyMode = PrivacyMode.getInstance().getMode();

        // retrieve the list of installed applications
        List<ApplicationInfo> apps = mPm.getInstalledApplications(0);

        if (apps == null) {
            apps = new ArrayList<ApplicationInfo>();
        }

        final Context context = getContext();

        String[] appGrid = GlobalData.getInstance().getApplicationGrid();
        ArrayList<AppModel> items = new ArrayList<>(appGrid.length);
        for (int i = 0; i < appGrid.length; i++) {
            String appName = appGrid[i];
            // Hide the application when being in private mode and if app is on hidden app list
            if(privacyMode == PrivacyMode.ACCESS_RESTRICTED && GlobalData.getInstance().isApplicationHiddenPrivate(appName)) {
                appName = "";
            }
            String searchString = GlobalData.getInstance().getPackageRegex(appName);
            ApplicationInfo appInfo = getApplicationInfoForSearch(apps, searchString);
            if(appInfo == null) {
                items.add(null);
            } else {
                AppModel app = new AppModel(context, appInfo);
                app.loadLabel(context);
                app.setInternalAppName(appName);
                items.add(app);
            }
        }

        return items;
    }

    @Override
    public void deliverResult(ArrayList<AppModel> apps) {
        if (isReset()) {
            // An async query came in while the loader is stopped.  We
            // don't need the result.
            if (apps != null) {
                onReleaseResources(apps);
            }
        }

        ArrayList<AppModel> oldApps = apps;
        mInstalledApps = apps;

        if (isStarted()) {
            // If the Loader is currently started, we can immediately
            // deliver its results.
            super.deliverResult(apps);
        }

        // At this point we can release the resources associated with
        // 'oldApps' if needed; now that the new result is delivered we
        // know that it is no longer in use.
        if (oldApps != null) {
            onReleaseResources(oldApps);
        }
    }

    @Override
    protected void onStartLoading() {
        if (mInstalledApps != null) {
            // If we currently have a result available, deliver it
            // immediately.
            deliverResult(mInstalledApps);
        }

        // watch for changes in app install and uninstall operation
        if (mPackageObserver == null) {
            mPackageObserver = new PackageIntentReceiver(this);
        }

        if (takeContentChanged() || mInstalledApps == null ) {
            // If the data has changed since the last time it was loaded
            // or is not currently available, start a load.
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        // Attempt to cancel the current load task if possible.
        cancelLoad();
    }

    @Override
    public void onCanceled(ArrayList<AppModel> apps) {
        super.onCanceled(apps);

        // At this point we can release the resources associated with 'apps'
        // if needed.
        onReleaseResources(apps);
    }

    @Override
    protected void onReset() {
        // Ensure the loader is stopped
        onStopLoading();

        // At this point we can release the resources associated with 'apps'
        // if needed.
        if (mInstalledApps != null) {
            onReleaseResources(mInstalledApps);
            mInstalledApps = null;
        }

        // Stop monitoring for changes.
        if (mPackageObserver != null) {
            getContext().unregisterReceiver(mPackageObserver);
            mPackageObserver = null;
        }
    }

    /**
     * Helper method to do the cleanup work if needed, for example if we're
     * using Cursor, then we should be closing it here
     *
     * @param apps
     */
    protected void onReleaseResources(ArrayList<AppModel> apps) {
        // do nothing
    }


    /**
     * Perform alphabetical comparison of application entry objects.
     */
    public static final Comparator<AppModel> ALPHA_COMPARATOR = new Comparator<AppModel>() {
        private final Collator sCollator = Collator.getInstance();
        @Override
        public int compare(AppModel object1, AppModel object2) {
            return sCollator.compare(object1.getLabel(), object2.getLabel());
        }
    };

    public ApplicationInfo getApplicationInfoForSearch(List<ApplicationInfo> apps, String search) {
        search = search.isEmpty() ? "" : ".*" + search + ".*";
        Pattern p = Pattern.compile(search);
        for (int i = 0; i < apps.size(); i++) {
            String pkg = apps.get(i).packageName;
            Matcher m = p.matcher(pkg);

            if (m.matches() && getContext().getPackageManager().getLaunchIntentForPackage(pkg) != null) {
                return apps.get(i);
            }
        }
        return null;
    }

}
