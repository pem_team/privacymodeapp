package com.pem.pemprivacymode;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Jump to lockscreen right after app launch
        Intent myIntent = new Intent(MainActivity.this, LockscreenActivity.class);
        MainActivity.this.startActivity(myIntent);

        // Jump to homescreen right after app launch
        //Intent myIntent = new Intent(MainActivity.this, HomescreenActivity.class);
        //MainActivity.this.startActivity(myIntent);
    }
}
