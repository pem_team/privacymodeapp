package com.pem.pemprivacymode;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.eftimoff.patternview.PatternView;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.view.IconicsImageView;
import com.mikepenz.ionicons_typeface_library.Ionicons;

public class LockscreenActivity extends AppCompatActivity {
    private PatternView patternView;
    private String patternStringPrivate = "000-000&001-000&002-000&003-000&003-001&003-002&003-003"; // Letter L from top left to bottom right
    private String patternStringGuest = "000-000&001-000&002-000&003-000&003-001&003-002&003-003&002-003&001-003&000-003"; // Letter U
    private String patternStringFull = "000-000&001-000&002-000&003-000&003-001&003-002"; // Letter L from top left to bottom right - 1

    private Toast sharingToast;
    private NotificationManager notificationManager;
    private int GUEST_NOTIFICATION_ID = 1011;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lockscreen);

        // Hide toolbar
        getSupportActionBar().hide();

        patternView = (PatternView) findViewById(R.id.patternView);
        patternView.setTactileFeedbackEnabled(false);
        //Toast.makeText(getApplicationContext(), "ENTER PATTERN", Toast.LENGTH_LONG).show();

        patternView.setOnPatternDetectedListener(new PatternView.OnPatternDetectedListener() {

            @Override
            public void onPatternDetected() {
                if (patternStringGuest == null) {
                    patternStringGuest = patternView.getPatternString();
                    Log.d("Lockscreen", "Pattern: " + patternStringGuest);
                    patternView.clearPattern();
                    return;
                }
                // Enter private mode
                if (patternStringPrivate.equals(patternView.getPatternString())) {
                    enterPrivateMode();
                    patternView.clearPattern();
                    return;
                }
                // Enter guest mode
                else if (patternStringGuest.equals(patternView.getPatternString())) {
                    enterGuestMode();
                    patternView.clearPattern();
                    return;
                }
                // Enter full access mode
                else if (patternStringFull.equals(patternView.getPatternString())) {
                    enterFullMode();
                    patternView.clearPattern();
                    return;
                }
                // Wrong pattern
                //Toast.makeText(getApplicationContext(), "Wrong pattern", Toast.LENGTH_SHORT).show();
                patternView.clearPattern();
            }
        });
    }

    private void enterPrivateMode() {
        //Toast.makeText(getApplicationContext(), "Pattern correct, switching to private mode", Toast.LENGTH_SHORT).show();
        PrivacyMode.getInstance().changeMode(PrivacyMode.ACCESS_RESTRICTED);
        Intent myIntent = new Intent(LockscreenActivity.this, HomescreenActivity.class);
        LockscreenActivity.this.startActivity(myIntent);
        removeGuestModeNotification();
    }

    private void enterGuestMode() {
        //Toast.makeText(getApplicationContext(), "Pattern correct, switching to guest mode", Toast.LENGTH_SHORT).show();
        showGuestModeToast(true);
        PrivacyMode.getInstance().changeMode(PrivacyMode.ACCESS_GUEST);
        Intent myIntent = new Intent(LockscreenActivity.this, HomescreenActivity.class);
        Intent myIntent2 = new Intent(LockscreenActivity.this, LockscreenActivity.class);
        postGuestModeNotification(myIntent2);
        LockscreenActivity.this.startActivity(myIntent);
    }

    private void enterFullMode() {
        //Toast.makeText(getApplicationContext(), "Pattern correct, switching to full access mode", Toast.LENGTH_SHORT).show();
        PrivacyMode.getInstance().changeMode(PrivacyMode.ACCESS_FULL);
        Intent myIntent = new Intent(LockscreenActivity.this, HomescreenActivity.class);
        //myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|
        //        Intent.FLAG_ACTIVITY_SINGLE_TOP);
        LockscreenActivity.this.startActivity(myIntent);
        removeGuestModeNotification();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("LockscreenActivity", "onNewIntent(): " + intent.getAction());
        // getIntent() should always return the most recent
        setIntent(intent);
    }

    public void postGuestModeNotification(Intent intent) {
        Bitmap icon = new IconicsDrawable(this).icon(Ionicons.Icon.ion_ios_body).color(Color.WHITE).sizeDp(48).toBitmap();
        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
        Notification n = new Notification.Builder(this)
                .setContentTitle("Guest Mode")
                .setContentText("Re-authenticate in full-access to disable")
                .setSmallIcon(R.drawable.guest_mode_icon_white)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .build();

        if(notificationManager == null) {
            notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        }

        notificationManager.notify(GUEST_NOTIFICATION_ID, n);
    }

    public void removeGuestModeNotification() {
        if(notificationManager != null) {
            notificationManager.cancel(GUEST_NOTIFICATION_ID);
        }
    }


    public void showGuestModeToast(boolean activated) {
        if (sharingToast == null) {
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.toast_sharing_layout,
                    (ViewGroup) findViewById(R.id.relativeLayout1));

            sharingToast = new Toast(this);
            sharingToast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            sharingToast.setDuration(Toast.LENGTH_SHORT);
            sharingToast.setView(view);
        }

        TextView modeTextView = (TextView) sharingToast.getView().findViewById(R.id.dialogTitle2);
        TextView activatedText = (TextView) sharingToast.getView().findViewById(R.id.dialogActivate2);
        IconicsImageView icon = (IconicsImageView) sharingToast.getView().findViewById(R.id.dialogIcon2);

        if (activated) {
            activatedText.setText("Enabled");
        } else {
            activatedText.setText("Disabled");
        }

        modeTextView.setText("Guest Mode");
        icon.setIcon("ion-ios-body");
        sharingToast.show();
    }
}
