package com.pem.pemprivacymode.AppActivities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.pem.pemprivacymode.Adapters.SettingsMainAdapter;
import com.pem.pemprivacymode.R;
import com.pem.pemprivacymode.SettingInformation;

import java.util.ArrayList;

public class SettingsPrivacealerActivity extends CustomAppActivity {
    private ListView privacealerOptionsList;

    private ArrayList<SettingInformation> mainSettings;
    private SettingsMainAdapter mainAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_privacealer);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Privacealer");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        myToolbar.setTitleTextColor(Color.BLACK);


        privacealerOptionsList = (ListView) findViewById(R.id.privacealerOptionsList);

        mainSettings = new ArrayList<>();

        SettingInformation s1 = new SettingInformation();
        s1.setKey("instantSharing");
        s1.setTitle("Instant Sharing");
        s1.setIconName("ion-flash");
        s1.setDescription("Freeze your screen immediately or restrict usage to current app");
        s1.setHasToggle(false);
        mainSettings.add(s1);

        SettingInformation s2 = new SettingInformation();
        s2.setKey("restrictedMode");
        s2.setTitle("Restricted Mode");
        s2.setIconName("ion-locked");
        s2.setDescription("Secretly hide apps or specific contents from closely related persons");
        s2.setHasToggle(false);
        mainSettings.add(s2);

        SettingInformation s3 = new SettingInformation();
        s3.setKey("guestMode");
        s3.setTitle("Guest Mode");
        s3.setIconName("ion-ios-body");
        s3.setDescription("Setup a public mode for strangers with hidden apps and contents");
        s3.setHasToggle(false);
        mainSettings.add(s3);

        mainAdapter = new SettingsMainAdapter(this, R.layout.settings_main_item, mainSettings);
        privacealerOptionsList.setAdapter(mainAdapter);

        privacealerOptionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                SettingInformation settingInformation = (SettingInformation) view.getTag();
                if(settingInformation == null || settingInformation.getKey() == null) {
                    return;
                }

                if(settingInformation.getKey().equals("restrictedMode")) {
                    Intent myIntent = new Intent(SettingsPrivacealerActivity.this, SettingsRestrictedModeActivity.class);
                    SettingsPrivacealerActivity.this.startActivity(myIntent);
                } else if(settingInformation.getKey().equals("guestMode")) {
                    Intent myIntent = new Intent(SettingsPrivacealerActivity.this, SettingsGuestModeActivity.class);
                    SettingsPrivacealerActivity.this.startActivity(myIntent);
                } else if(settingInformation.getKey().equals("instantSharing")) {
                    Intent myIntent = new Intent(SettingsPrivacealerActivity.this, SettingsInstantSharingActivity.class);
                    SettingsPrivacealerActivity.this.startActivity(myIntent);
                }

            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void privacyModeChanged(int newMode) {
        super.privacyModeChanged(newMode);
        // Refresh the activity content
        //renderContent();
    }


}