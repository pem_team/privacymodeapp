package com.pem.pemprivacymode.AppActivities;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.iconics.view.IconicsImageView;
import com.pem.pemprivacymode.PrivacyMode;
import com.pem.pemprivacymode.R;
import com.pem.pemprivacymode.utils.InstantSharingDialog;
import com.pem.pemprivacymode.utils.LockscreenUtils;

public class CustomAppActivity extends AppCompatActivity implements PrivacyMode.OnPrivacyModeChangeListener, LockscreenUtils.OnLockStatusChangedListener {
    private LockscreenUtils mLockscreenUtils;
    private boolean initialized = false;
    public int privacyMode = -1;
    public final static int REQUEST_CODE = 10101;
    private Toast sharingToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_chrome);

        PrivacyMode.getInstance().setListener(this);
        privacyMode = PrivacyMode.getInstance().getMode();

        mLockscreenUtils = new LockscreenUtils();

        initialized = true;
    }

    @Override
    public void privacyModeChanged(int newMode) {
        int oldMode = privacyMode;
        privacyMode = newMode;
        if(privacyMode == PrivacyMode.ACCESS_RESTRICTED) {
            //attemptScreenLock();
        }
        if(privacyMode == PrivacyMode.ACCESS_FULL) {
            //********Auskommentieren????*****************/
            showInstantSharingToast(oldMode, false);
            //View parentLayout = findViewById(R.id.root_view);
            //Snackbar snackbar = Snackbar
            //        .make(parentLayout, "Instant Sharing is OFF", Snackbar.LENGTH_SHORT);
            //snackbar.show();
            //Toast.makeText(getApplicationContext(), "Instant Sharing OFF", Toast.LENGTH_SHORT).show();
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
        if(privacyMode == PrivacyMode.ACCESS_INSTANT_FREEZE) {
            showInstantSharingToast(privacyMode, true);
            //View parentLayout = findViewById(R.id.root_view);
            //Snackbar snackbar = Snackbar
            //        .make(parentLayout, "Instant Sharing is ON", Snackbar.LENGTH_SHORT);
            //snackbar.show();
            //Toast.makeText(getApplicationContext(), "Instant Sharing ON", Toast.LENGTH_SHORT).show();
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else if(privacyMode == PrivacyMode.ACCESS_INSTANT_APP) {
            showInstantSharingToast(privacyMode, true);
        } else if(privacyMode == PrivacyMode.ACCESS_RESTRICTED) {
            showInstantSharingToast(privacyMode, true);
        }
    }

    public void showInstantSharingToast(int mode, boolean activated) {
        if(!initialized) {
            return;
        }
        if(sharingToast == null) {
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.toast_sharing_layout,
                    (ViewGroup) findViewById(R.id.relativeLayout1));

            sharingToast = new Toast(this);
            sharingToast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            sharingToast.setDuration(Toast.LENGTH_SHORT);
            sharingToast.setView(view);
        }



        TextView modeTextView = (TextView) sharingToast.getView().findViewById(R.id.dialogTitle2);
        TextView activatedText = (TextView) sharingToast.getView().findViewById(R.id.dialogActivate2);
        IconicsImageView icon = (IconicsImageView) sharingToast.getView().findViewById(R.id.dialogIcon2);

        if(activated) {
            activatedText.setText("Enabled");
        } else {
            activatedText.setText("Disabled");
        }

        if(mode == PrivacyMode.ACCESS_INSTANT_FREEZE) {
            modeTextView.setText("Freeze Screen");
            icon.setIcon("ion-qr-scanner");

        } else if(mode == PrivacyMode.ACCESS_INSTANT_APP) {
            modeTextView.setText("Restrict To Current App");
            icon.setIcon("ion-qr-scanner");

        } else if(mode == PrivacyMode.ACCESS_RESTRICTED) {
            modeTextView.setText("Restricted Mode");
            icon.setIcon("ion-locked");
            if(activated) {
                icon.setIcon("ion-locked");
            } else {
                icon.setIcon("ion-unlocked");
            }
        }

        //SeekBar volumeBar = (SeekBar) sharingToast.getView().findViewById(R.id.volumeBar);
        //volumeBar.setProgress(GLOBAL_VOLUME);
        sharingToast.show();
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if(privacyMode == PrivacyMode.ACCESS_FULL) {
                //PrivacyMode.getInstance().changeMode(PrivacyMode.ACCESS_INSTANT_FREEZE);
                InstantSharingDialog dialog = new InstantSharingDialog(this);
                dialog.show();
            } else {
                PrivacyMode.getInstance().changeMode(PrivacyMode.ACCESS_FULL);
            }
            return true;
        }
        return super.onKeyLongPress(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        if(privacyMode == PrivacyMode.ACCESS_INSTANT_FREEZE || privacyMode == PrivacyMode.ACCESS_INSTANT_APP) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onLockStatusChanged(boolean isLocked) {
        /*if (!isLocked) {
        }*/
    }

    @Override
    public void onPause() {
        super.onPause();
        initialized = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        initialized = true;
    }

}

