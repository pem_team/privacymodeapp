package com.pem.pemprivacymode.AppActivities;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;

import com.pem.pemprivacymode.Adapters.SettingsAppListAdapter;
import com.pem.pemprivacymode.Homescreen.AppModel;
import com.pem.pemprivacymode.R;

import java.util.ArrayList;
import java.util.List;

public class SettingsHideAppContentsActivity extends CustomAppActivity {
    private ImageView hideContentWhatsAppIcon;
    private ImageView hideContentGalleryIcon;
    private ImageView hideContentBrowserIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_hide_app_contents);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        myToolbar.setTitleTextColor(Color.WHITE);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        hideContentWhatsAppIcon = (ImageView) findViewById(R.id.hideContentWhatsAppIcon);
        hideContentGalleryIcon = (ImageView) findViewById(R.id.hideContentGalleryIcon);
        hideContentBrowserIcon = (ImageView) findViewById(R.id.hideContentBrowserIcon);

        try {
            Drawable icon = getPackageManager().getApplicationIcon("com.whatsapp");
            hideContentWhatsAppIcon.setImageDrawable(icon);
            Drawable icon2 = getPackageManager().getApplicationIcon("com.google.android.apps.photos");
            hideContentGalleryIcon.setImageDrawable(icon2);
            Drawable icon3 = getPackageManager().getApplicationIcon("com.android.chrome");
            hideContentBrowserIcon.setImageDrawable(icon3);
        } catch(PackageManager.NameNotFoundException e) {

        }


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void privacyModeChanged(int newMode) {
        super.privacyModeChanged(newMode);
        // Refresh the activity content
    }


}
