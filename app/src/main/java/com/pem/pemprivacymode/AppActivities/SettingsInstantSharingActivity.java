package com.pem.pemprivacymode.AppActivities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import com.pem.pemprivacymode.Adapters.SettingsMainAdapter;
import com.pem.pemprivacymode.R;
import com.pem.pemprivacymode.SettingInformation;

import java.util.ArrayList;

public class SettingsInstantSharingActivity extends CustomAppActivity {
    private ListView privacealerOptionsList;

    private ArrayList<SettingInformation> mainSettings;
    private SettingsMainAdapter mainAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_instant);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        myToolbar.setTitleTextColor(Color.WHITE);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#1DB34C"));
        }

        privacealerOptionsList = (ListView) findViewById(R.id.privacealerOptionsList2);

        mainSettings = new ArrayList<>();

        SettingInformation s1 = new SettingInformation();
        s1.setKey("keyShortcut");
        s1.setTitle("Key shortcut");
        s1.setIconName("ion-pricetag");
        s1.setDescription("Long back button press");
        s1.setHasToggle(false);
        mainSettings.add(s1);

        SettingInformation s2 = new SettingInformation();
        s2.setKey("hideAllContents");
        s2.setTitle("Always show dialog");
        s2.setIconName("ion-android-map");
        s2.setDescription("If selected the options dialog is\nalways shown");
        s2.setHasToggle(true);
        s2.setValueBool(true);
        mainSettings.add(s2);

        SettingInformation s3 = new SettingInformation();
        s3.setKey("showNotification");
        s3.setTitle("Direct action");
        s3.setIconName("ion-share");
        s3.setDescription("Perform the selected direct action\nwhen dialog is disabled");
        mainSettings.add(s3);

        SettingInformation s4 = new SettingInformation();
        s4.setKey("showNotification");
        s4.setTitle("Hide action messages");
        s4.setIconName("ion-eye-disabled");
        s4.setDescription("By default, a note is shown when\nentering a mode. Enable for better secrecy");
        s4.setHasToggle(true);
        s4.setValueBool(false);
        mainSettings.add(s4);

        mainAdapter = new SettingsMainAdapter(this, R.layout.settings_main_item, mainSettings);
        privacealerOptionsList.setAdapter(mainAdapter);

        privacealerOptionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                SettingInformation settingInformation = (SettingInformation) view.getTag();
                if(settingInformation == null || settingInformation.getKey() == null) {
                    return;
                }

            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void privacyModeChanged(int newMode) {
        super.privacyModeChanged(newMode);
        // Refresh the activity content
        //renderContent();
    }


}