package com.pem.pemprivacymode.AppActivities;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.pem.pemprivacymode.Adapters.ImageAdapter;
import com.pem.pemprivacymode.PrivacyMode;
import com.pem.pemprivacymode.R;

import java.util.ArrayList;
import java.util.Arrays;

public class SwipeViewActivity extends CustomAppActivity {
    Integer [] pageData;	//Stores the text to swipe.
    LayoutInflater inflater;	//Used to create individual pages
    ViewPager vp;	//Reference to class to swipe views
    Intent intent;
    Integer startImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipeview);
        intent = getIntent();
        //Get the data to be swiped through
        Object[] s = (Object[]) intent.getSerializableExtra("mThumbIds");
        pageData = Arrays.copyOf(s, s.length, Integer[].class);
        startImage=intent.getIntExtra("image", 0);
        //get an inflater to be used to create single pages
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //Reference ViewPager defined in activity
        vp=(ViewPager)findViewById(R.id.viewPager);
        //set the adapter that will create the individual pages
        vp.setAdapter(new MyPagesAdapter());

        /*
        switch (privacyMode){
            case PrivacyMode.ACCESS_FULL:
                pageData = new Integer[] {
                        R.drawable.bed,
                        R.drawable.urlaub1,
                        R.drawable.urlaub2,
                        R.drawable.urlaub3,
                        R.drawable.urlaub4,
                        R.drawable.urlaub5,
                        R.drawable.urlaub6,
                        R.drawable.urlaub7,
                        R.drawable.urlaub8,
                        R.drawable.urlaub9,
                        R.drawable.urlaub10,
                        R.drawable.urlaub11,
                        R.drawable.urlaub12,
                        R.drawable.urlaub13
                };
                break;
            case PrivacyMode.ACCESS_RESTRICTED:
                pageData = new Integer[] {
                        R.drawable.bed,
                        R.drawable.urlaub1,
                        R.drawable.urlaub2,
                        R.drawable.urlaub3,
                        R.drawable.urlaub4,
                        R.drawable.urlaub5,
                        R.drawable.urlaub7,
                        R.drawable.urlaub8,
                        R.drawable.urlaub9,
                        R.drawable.urlaub10,
                        R.drawable.urlaub11,
                        R.drawable.urlaub12,
                        R.drawable.urlaub13
                };
                break;
            case PrivacyMode.ACCESS_GUEST:

                break;
            default:
                pageData = new Integer[] {
                        R.drawable.bed,
                        R.drawable.urlaub1,
                        R.drawable.urlaub2,
                        R.drawable.urlaub3,
                        R.drawable.urlaub4,
                        R.drawable.urlaub5,
                        R.drawable.urlaub6,
                        R.drawable.urlaub7,
                        R.drawable.urlaub8,
                        R.drawable.urlaub9,
                        R.drawable.urlaub10,
                        R.drawable.urlaub11,
                        R.drawable.urlaub12,
                        R.drawable.urlaub13,
                };
                break;
        }
        vp.setAdapter(new MyPagesAdapter());*/
    }

    /*
    @Override
    public void privacyModeChanged(int newMode) {
        super.privacyModeChanged(newMode);
        // Refresh the activity content
        finish();
        startActivity(getIntent());
    }*/

    //Implement PagerAdapter Class to handle individual page creation
    class MyPagesAdapter extends PagerAdapter {
        @Override
        public int getCount() {
            //Return total pages, here one for each data item
            return pageData.length;
        }
        //Create the given page (indicated by position)
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View page = inflater.inflate(R.layout.page, null);
            ((ImageView)page.findViewById(R.id.image)).setImageResource(pageData[position]);
            //Add the page to the front of the queue
            ((ViewPager) container).addView(page, 0);
            return page;
        }
        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            //See if object from instantiateItem is related to the given view
            //required by API
            return arg0==(View)arg1;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
            object=null;
        }




    }



}


