package com.pem.pemprivacymode.AppActivities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.pem.pemprivacymode.PrivacyMode;
import com.pem.pemprivacymode.R;

public class DialerActivity extends CustomAppActivity implements DialerNumbersFragment.OnFragmentInteractionListener {

    private RelativeLayout myLayout;
    private final Fragment myFragment = new DialerNumbersFragment();
    private int bg = 0;

    //dialer states
    private final static int STATE_OVERVIEW = 0;
    private final static int STATE_FRAG = 1;
    private final static int STATE_CALL = 2;

    //current state
    private int myState = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialer);

        // Render the activity content
        renderContent();

        final ImageButton dialerBTN = (ImageButton) findViewById(R.id.dialerBTN);
        if (dialerBTN != null) {
            dialerBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // get an instance of FragmentTransaction from your Activity
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                    //add a fragment
                    fragmentTransaction.replace(R.id.dialerFrameL, myFragment, "dialerFragment");


                    if (myState == DialerActivity.STATE_OVERVIEW) {
                        myState = DialerActivity.STATE_FRAG;
                        fragmentTransaction.show(myFragment);
                        myLayout.setBackgroundResource(0);
                        dialerBTN.setImageResource(R.drawable.dialer_btn_call);

                    } else if (myState == DialerActivity.STATE_FRAG) {
                        myState = DialerActivity.STATE_CALL;
                        fragmentTransaction.hide(myFragment);
                        myLayout.setBackgroundResource(R.drawable.dialer_call);
                        dialerBTN.setImageResource(R.drawable.dialer_btn_hang);

                    } else if (myState == DialerActivity.STATE_CALL) {
                        myState = DialerActivity.STATE_OVERVIEW;
                        if (myFragment != null) {
                            resetDialerNumbers(myFragment);
                        }
                        myLayout.setBackgroundResource(bg);
                        dialerBTN.setImageResource(R.drawable.dialer_btn_num);

                    }

                    fragmentTransaction.commit();
                }
            });
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void privacyModeChanged(int newMode) {
        super.privacyModeChanged(newMode);
        // Refresh the activity content
        renderContent();
    }

    private void renderContent() {
        myLayout = (RelativeLayout) findViewById(R.id.dialerLayout);
        switch (privacyMode) {
            case PrivacyMode.ACCESS_FULL:
                this.bg = R.drawable.dialerbg_full;
                break;
            case PrivacyMode.ACCESS_RESTRICTED:
                this.bg = R.drawable.dialerbg_rest;
                break;
            case PrivacyMode.ACCESS_GUEST:
                this.bg = R.drawable.dialerbg_guest;
                break;
            default:
                this.bg = R.drawable.dialerbg_full;
                break;
        }
        myLayout.setBackgroundResource(bg);
    }

    public void resetDialerNumbers(Fragment f) {
        DialerNumbersFragment frag = (DialerNumbersFragment) f;
        if (frag != null) {
            frag.resetNumbers();
        }
    }
}
