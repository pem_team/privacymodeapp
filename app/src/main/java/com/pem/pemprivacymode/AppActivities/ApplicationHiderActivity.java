package com.pem.pemprivacymode.AppActivities;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.pem.pemprivacymode.Adapters.SettingsAppListAdapter;
import com.pem.pemprivacymode.Homescreen.AppModel;
import com.pem.pemprivacymode.PrivacyMode;
import com.pem.pemprivacymode.R;

import java.util.ArrayList;
import java.util.List;

public class ApplicationHiderActivity extends CustomAppActivity {
    private ListView listView;
    private SettingsAppListAdapter adapter;

    private PackageManager mPm;
    private ArrayList<AppModel> appModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_application_hider);

        Bundle b = getIntent().getExtras();
        int mode = -1;
        if(b != null) {
            mode = b.getInt("mode");
        }

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        myToolbar.setTitleTextColor(Color.WHITE);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        // If we come from guest mode settings
        if(mode == 1) {
            TextView headerTitle = (TextView) findViewById(R.id.settingsHeaderText241);
            TextView headerText = (TextView) findViewById(R.id.settingsHeaderSubText241);
            headerTitle.setText("Show Applications");
            headerText.setText("Only selected apps will be shown on the homescreen when guest mode is enabled");
        }



        // retrieve the list of installed applications
        mPm = this.getPackageManager();
        List<ApplicationInfo> apps = mPm.getInstalledApplications(0);
        appModelList = new ArrayList<>();

        for (int i = 0; i < apps.size(); i++) {
            String pkg = apps.get(i).packageName;
            if (mPm.getLaunchIntentForPackage(pkg) != null) {
                ApplicationInfo appInfo = apps.get(i);
                AppModel app = new AppModel(this, appInfo);
                app.loadLabel(this);
                appModelList.add(app);
            }
        }

        adapter = new SettingsAppListAdapter(this);
        adapter.setData(appModelList);
        listView = (ListView) findViewById(R.id.appListView);
        listView.setAdapter(adapter);


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void privacyModeChanged(int newMode) {
        super.privacyModeChanged(newMode);
        // Refresh the activity content
    }


}


