package com.pem.pemprivacymode.AppActivities;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.pem.pemprivacymode.PrivacyMode;
import com.pem.pemprivacymode.R;

public class ChromeActivity extends CustomAppActivity {

    ImageButton menuButton;
    PopupMenu popup;
    Menu popupMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chrome);

        menuButton = (ImageButton) findViewById(R.id.menuButton);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu(v);
            }
        });

        popup = new PopupMenu(this, menuButton);
        popup.inflate(R.menu.chrome_menu);
        popupMenu = popup.getMenu();
        inflateMenu();
        invalidateOptionsMenu();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.history:

                        return true;

                    default:
                        return false;
                }
            }
        });
    }

    public void showMenu(View v) {
        popup.show();
    }

    @Override
    public void privacyModeChanged(int newMode) {
        super.privacyModeChanged(newMode);
        inflateMenu();
    }

    private void inflateMenu() {
        popupMenu.findItem(R.id.history).setVisible(true);
        switch (privacyMode) {
            case PrivacyMode.ACCESS_FULL:
                popupMenu.findItem(R.id.history).setEnabled(true);
                break;
            case PrivacyMode.ACCESS_RESTRICTED:
                popupMenu.findItem(R.id.history).setVisible(false);
                break;
            case PrivacyMode.ACCESS_GUEST:
                popupMenu.findItem(R.id.history).setEnabled(false);
                break;
            default:
                popupMenu.findItem(R.id.history).setEnabled(true);
                break;
        }
    }
}
