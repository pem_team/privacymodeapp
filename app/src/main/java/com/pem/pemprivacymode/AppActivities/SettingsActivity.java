package com.pem.pemprivacymode.AppActivities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.pem.pemprivacymode.Adapters.SettingsMainAdapter;
import com.pem.pemprivacymode.HomescreenActivity;
import com.pem.pemprivacymode.PrivacyMode;
import com.pem.pemprivacymode.R;
import com.pem.pemprivacymode.SettingInformation;

import java.util.ArrayList;

public class SettingsActivity extends CustomAppActivity {
    private ListView networkSettingsList;
    private ListView generalSettingsList;

    private ArrayList<SettingInformation> mainSettings;
    private ArrayList<SettingInformation> generalSettings;
    private SettingsMainAdapter mainAdapter;
    private SettingsMainAdapter generalAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Settings");
        myToolbar.setTitleTextColor(Color.WHITE);


        networkSettingsList = (ListView) findViewById(R.id.networkSettingsList);
        generalSettingsList = (ListView) findViewById(R.id.generalSettingsList);

        mainSettings = new ArrayList<>();

        SettingInformation s1 = new SettingInformation();
        s1.setKey("wifi");
        s1.setTitle("Wi-Fi");
        s1.setIconName("ion-wifi");
        //s1.setDescription("If this is checked all other tabs are paused when playing");
        s1.setHasToggle(true);
        mainSettings.add(s1);

        SettingInformation s2 = new SettingInformation();
        s2.setKey("bluetooth");
        s2.setTitle("Bluetooth");
        s2.setIconName("ion-bluetooth");
        s2.setHasToggle(false);
        mainSettings.add(s2);

        SettingInformation s3 = new SettingInformation();
        s3.setKey("dataUsage");
        s3.setTitle("Data usage");
        s3.setIconName("ion-archive");
        s3.setHasToggle(false);
        mainSettings.add(s3);

        mainAdapter = new SettingsMainAdapter(this, R.layout.settings_main_item, mainSettings);
        networkSettingsList.setAdapter(mainAdapter);

        networkSettingsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                SettingInformation settingInformation = (SettingInformation) view.getTag();


            }
        });


        generalSettings = new ArrayList<>();

        SettingInformation s11 = new SettingInformation();
        s11.setKey("privacealer");
        s11.setTitle("Privacealer");
        s11.setIconName("ion-aperture");
        s11.setDescription("Configure your privacy modes");
        generalSettings.add(s11);

        SettingInformation s12 = new SettingInformation();
        s12.setKey("sounds");
        s12.setTitle("Sounds & notifications");
        s12.setIconName("ion-android-notifications");
        generalSettings.add(s12);

        SettingInformation s13 = new SettingInformation();
        s13.setKey("display");
        s13.setTitle("Display");
        s13.setIconName("ion-contrast");
        generalSettings.add(s13);

        generalAdapter = new SettingsMainAdapter(this, R.layout.settings_main_item, generalSettings);
        generalSettingsList.setAdapter(generalAdapter);

        generalSettingsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                SettingInformation settingInformation = (SettingInformation) view.getTag();
                if(settingInformation.getKey() != null && settingInformation.getKey().equals("privacealer")) {
                    Intent myIntent = new Intent(SettingsActivity.this, SettingsPrivacealerActivity.class);
                    SettingsActivity.this.startActivity(myIntent);
                }


            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void privacyModeChanged(int newMode) {
        super.privacyModeChanged(newMode);
        // Refresh the activity content
        //renderContent();
    }


}


