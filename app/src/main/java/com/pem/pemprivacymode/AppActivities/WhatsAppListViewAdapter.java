package com.pem.pemprivacymode.AppActivities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pem.pemprivacymode.R;

import java.util.ArrayList;


class WhatsAppListViewAdapter extends BaseAdapter {
    String [] name;
    String [] message;
    String [] date;
    Context context;
    int [] profilePicture;

    private static LayoutInflater inflater=null;

    public WhatsAppListViewAdapter(WhatsAppActivity whatsAppActivity,  int[] profilePictures, String[] names, String[] messages, String[] dates) {
        context=whatsAppActivity;
        name=names;
        message=messages;
        date=dates;
        profilePicture=profilePictures;

        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return name.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView textViewName;
        TextView textViewMessage;
        TextView textViewDate;
        ImageView imageView;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.whatsapp_row, null);

        holder.textViewName=(TextView) rowView.findViewById(R.id.textViewName);
        holder.textViewMessage=(TextView) rowView.findViewById(R.id.textViewLastMessage);
        holder.textViewDate=(TextView) rowView.findViewById(R.id.textViewDate);
        holder.imageView=(ImageView) rowView.findViewById(R.id.profilePicture);

        holder.textViewName.setText(name[position]);
        holder.textViewMessage.setText(message[position]);
        holder.textViewDate.setText(date[position]);
        holder.imageView.setImageResource(profilePicture[position]);

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name[position].equals("Ben")){
                    Intent intent = new Intent(context, DisplayMessageActivity.class);
                    context.startActivity(intent);
                }
            }
        });
        return rowView;
    }


}
