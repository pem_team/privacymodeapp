package com.pem.pemprivacymode.AppActivities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.pem.pemprivacymode.Adapters.ImageAdapter;
import com.pem.pemprivacymode.PrivacyMode;
import com.pem.pemprivacymode.R;

/**
 * Created by marei_000 on 03.07.2016.
 */
public class GalleryActivity2 extends CustomAppActivity{
    GridView gridview;
    private ImageAdapter adapter;
    public Integer[] mThumbIds;
    Integer image;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery2);

        adapter = new ImageAdapter(this, mThumbIds);
        gridview  = (GridView) findViewById(R.id.gridview);
        //gridview.setAdapter(adapter);

        renderContent();



        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                //Toast.makeText(GalleryActivity2.this, "" + position,Toast.LENGTH_SHORT).show();
                for (position = 0; position < mThumbIds.length; position++) {
                    image = mThumbIds[position];
                }
                Intent intent = new Intent(GalleryActivity2.this, SwipeViewActivity.class);
                intent.putExtra("image", image);
                intent.putExtra("mThumbIds", mThumbIds);
                startActivity(intent);
            }
        });
    }

    @Override
    public void privacyModeChanged(int newMode) {
        super.privacyModeChanged(newMode);
        // Refresh the activity content
        renderContent();
    }

    private void renderContent() {
        switch (privacyMode){
            case PrivacyMode.ACCESS_FULL:
                mThumbIds = new Integer[] {
                        R.drawable.bed,
                        R.drawable.urlaub1,
                        R.drawable.urlaub2,
                        R.drawable.urlaub3,
                        R.drawable.urlaub4,
                        R.drawable.urlaub5,
                        R.drawable.urlaub6,
                        R.drawable.urlaub7,
                        R.drawable.urlaub8,
                        R.drawable.urlaub9,
                        R.drawable.urlaub10,
                        R.drawable.urlaub11,
                        R.drawable.urlaub12,
                        R.drawable.urlaub13
                };
                break;
            case PrivacyMode.ACCESS_RESTRICTED:
                mThumbIds = new Integer[] {
                        R.drawable.bed,
                        R.drawable.urlaub1,
                        R.drawable.urlaub2,
                        R.drawable.urlaub3,
                        R.drawable.urlaub4,
                        R.drawable.urlaub5,
                        R.drawable.urlaub7,
                        R.drawable.urlaub8,
                        R.drawable.urlaub9,
                        R.drawable.urlaub10,
                        R.drawable.urlaub11,
                        R.drawable.urlaub12,
                        R.drawable.urlaub13
                };
                break;
            case PrivacyMode.ACCESS_GUEST:

                break;
            default:
                mThumbIds = new Integer[] {
                        R.drawable.bed,
                        R.drawable.urlaub1,
                        R.drawable.urlaub2,
                        R.drawable.urlaub3,
                        R.drawable.urlaub4,
                        R.drawable.urlaub5,
                        R.drawable.urlaub6,
                        R.drawable.urlaub7,
                        R.drawable.urlaub8,
                        R.drawable.urlaub9,
                        R.drawable.urlaub10,
                        R.drawable.urlaub11,
                        R.drawable.urlaub12,
                        R.drawable.urlaub13,
                };
                break;
        }
        adapter = new ImageAdapter(this, mThumbIds);
        gridview  = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(adapter);

    }


}
