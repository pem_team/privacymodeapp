package com.pem.pemprivacymode.AppActivities;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;

import com.pem.pemprivacymode.R;
import com.pem.pemprivacymode.Adapters.*;

public class SettingsHideContactsActivity extends CustomAppActivity {
    private ListView listView;
    private ContactsListViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_hide_contacts);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        myToolbar.setTitleTextColor(Color.WHITE);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        int[] profilePictures= new int[]{R.drawable.profile1,R.drawable.profile3, R.drawable.profile4,R.drawable.profile5,R.drawable.profile6, R.drawable.profile7,R.drawable.profile8,R.drawable.profile9, R.drawable.profile10,R.drawable.profile11};
        String [] names = new String[]{"Eva Berger", "Sabrina Becker", "Sarah Roberts", "Ben", "Paul Evers", "Hannah Miles", "Jacob Black", "Cedric Diggory", "Lauren Brown", "Christian Grey"};

        adapter = new ContactsListViewAdapter(this, profilePictures, names);
        listView = (ListView) findViewById(R.id.listView22);
        listView.setAdapter(adapter);


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void privacyModeChanged(int newMode) {
        super.privacyModeChanged(newMode);
        // Refresh the activity content
    }


}
