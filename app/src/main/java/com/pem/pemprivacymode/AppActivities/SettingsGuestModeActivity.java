package com.pem.pemprivacymode.AppActivities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Switch;

import com.pem.pemprivacymode.Adapters.SettingsMainAdapter;
import com.pem.pemprivacymode.R;
import com.pem.pemprivacymode.SettingInformation;

import java.util.ArrayList;

public class SettingsGuestModeActivity extends CustomAppActivity {
    private ListView privacealerOptionsList;

    private ArrayList<SettingInformation> mainSettings;
    private SettingsMainAdapter mainAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_guest);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        myToolbar.setTitleTextColor(Color.WHITE);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#1976B0"));
        }

        privacealerOptionsList = (ListView) findViewById(R.id.privacealerOptionsList2);

        mainSettings = new ArrayList<>();

        SettingInformation s1 = new SettingInformation();
        s1.setKey("showApps");
        s1.setTitle("Show applications");
        s1.setIconName("ion-android-apps");
        s1.setDescription("Select applications that should be available on the homescreen. Other applications won't be accessible");
        s1.setHasToggle(false);
        mainSettings.add(s1);

        SettingInformation s2 = new SettingInformation();
        s2.setKey("hideAllContents");
        s2.setTitle("Hide all contents");
        s2.setIconName("ion-android-list");
        s2.setDescription("If selected all contents in every\napplication will be hidden");
        s2.setHasToggle(true);
        mainSettings.add(s2);

        SettingInformation s3 = new SettingInformation();
        s3.setKey("showNotification");
        s3.setTitle("Show notification");
        s3.setIconName("ion-android-notifications");
        s3.setDescription("If selected a notification is shown\nin the status bar when guest mode\nis enabled.");
        s3.setHasToggle(true);
        s3.setValueBool(true);
        mainSettings.add(s3);

        mainAdapter = new SettingsMainAdapter(this, R.layout.settings_main_item, mainSettings);
        privacealerOptionsList.setAdapter(mainAdapter);

        privacealerOptionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                SettingInformation settingInformation = (SettingInformation) view.getTag();
                if(settingInformation == null || settingInformation.getKey() == null) {
                    return;
                }

                if(settingInformation.getKey().equals("showApps")) {
                    Intent myIntent = new Intent(SettingsGuestModeActivity.this, ApplicationHiderActivity.class);
                    Bundle b = new Bundle();
                    b.putInt("mode", 1);
                    myIntent.putExtras(b);
                    SettingsGuestModeActivity.this.startActivity(myIntent);
                }

            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void privacyModeChanged(int newMode) {
        super.privacyModeChanged(newMode);
        // Refresh the activity content
        //renderContent();
    }


}