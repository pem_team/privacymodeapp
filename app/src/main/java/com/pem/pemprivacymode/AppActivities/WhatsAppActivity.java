package com.pem.pemprivacymode.AppActivities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pem.pemprivacymode.LockscreenActivity;
import com.pem.pemprivacymode.PrivacyMode;
import com.pem.pemprivacymode.R;

import java.util.ArrayList;

public class WhatsAppActivity extends CustomAppActivity {
    private ListView listView;
    private WhatsAppListViewAdapter adapter;
    Context context;
    ArrayList chats;

    public int [] profilePictures;
    public String [] names;
    public String [] messages;
    public String [] dates ={"15:12","14:43","11:37","21:38","21:38","01.07","01.07","01.07","30.06.", "30.06.","27.06."};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whats_app);

        profilePictures= new int[]{};
        names = new String[]{};
        messages = new String[]{};

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("WhatsApp");
        myToolbar.setTitleTextColor(Color.WHITE);

        adapter = new WhatsAppListViewAdapter(this, profilePictures, names, messages, dates);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);

        renderContent();

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void privacyModeChanged(int newMode) {
        super.privacyModeChanged(newMode);
        // Refresh the activity content
        renderContent();
    }

    private void renderContent() {
        switch (privacyMode){
            case PrivacyMode.ACCESS_FULL:
                profilePictures= new int[]{R.drawable.profile1,R.drawable.happy_birthday,R.drawable.profile3, R.drawable.profile4,R.drawable.profile5,R.drawable.profile6, R.drawable.profile7,R.drawable.profile8,R.drawable.profile9, R.drawable.profile10,R.drawable.profile11};
                names = new String[]{"Eva Berger", "Sarahs Birthday Party", "Sabrina Becker", "Sarah Roberts", "Ben", "Paul Evers", "Hannah Miles", "Jacob Black", "Cedric Diggory", "Lauren Brown", "Christian Grey"};
                messages = new String[]{"Ok, see you","I could make some cake pops","she said maybe tomorrow", "Ok :)","..are you up for some Netflix&Chill?","I'm in Munich thh whole summer", "I'm not in class today","Ciao","Hey, how are you doing?", "Yes, you too","Good luck tomorrow","It was my brother"};
                break;
            case PrivacyMode.ACCESS_RESTRICTED:
                profilePictures= new int[]{R.drawable.profile1,R.drawable.profile3, R.drawable.profile4,R.drawable.profile5,R.drawable.profile6, R.drawable.profile7,R.drawable.profile8,R.drawable.profile9, R.drawable.profile10,R.drawable.profile11};
                names = new String[]{"Eva Berger", "Sabrina Bescker", "Sarah Roberts", "Ben", "Paul Evers", "Hannah Miles", "Jacob Black", "Cedric Diggory", "Lauren Brown", "Christian Grey"};
                messages = new String[]{"Ok, see you","she said maybe tomorrow", "Ok :)","..are you up for some Netflix&Chill?","I'm in Munich thh whole summer", "I'm not in class today","Ciao","Hey, how are you doing?", "Yes, you too","Good luck tomorrow","It was my brother"};
                break;
            case PrivacyMode.ACCESS_GUEST:
                profilePictures= new int[]{};
                names = new String[]{};
                messages = new String[]{};
                break;
            default:
                profilePictures= new int[]{R.drawable.profile1,R.drawable.happy_birthday,R.drawable.profile3, R.drawable.profile4,R.drawable.profile5,R.drawable.profile6, R.drawable.profile7,R.drawable.profile8,R.drawable.profile9, R.drawable.profile10,R.drawable.profile11};
                names = new String[]{"Eva Berger", "Sarahs Birthday Party", "Sabrina Becker", "Sarah Roberts", "Ben", "Paul Evers", "Hannah Miles", "Jacob Black", "Cedric Diggory", "Lauren Brown", "Christian Grey"};
                messages = new String[]{"Ok, see you","I could make some cake pops","she said maybe tomorrow", "Ok :)","..are you up for some Netflix&Chill?","I'm in Munich thh whole summer", "I'm not in class today","Ciao","Hey, how are you doing?", "Yes, you too","Good luck tomorrow","It was my brother"};
                break;
        }

        adapter = new WhatsAppListViewAdapter(this, profilePictures, names, messages, dates);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
    }


}


