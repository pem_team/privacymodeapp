package com.pem.pemprivacymode;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.pem.pemprivacymode.Adapters.AppIconAdapter;
import com.pem.pemprivacymode.Homescreen.AppsGridFragment;
import com.pem.pemprivacymode.utils.HomeKeyListener;

import java.util.HashMap;
import java.util.Map;


public class HomescreenActivity extends FragmentActivity implements PrivacyMode.OnPrivacyModeChangeListener {

    private AppsGridFragment gridFragment;
    private int privacyMode = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homescreen);

        PrivacyMode.getInstance().setListener(this);
        privacyMode = PrivacyMode.getInstance().getMode();

        gridFragment = (AppsGridFragment) getSupportFragmentManager().findFragmentById(R.id.apps_grid);
    }

    @Override
    public void privacyModeChanged(int newMode) {
        privacyMode = newMode;
        // Refresh the activity content
        gridFragment.privacyModeChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(privacyMode != PrivacyMode.getInstance().getMode()) {
            privacyMode = PrivacyMode.getInstance().getMode();
            privacyModeChanged(privacyMode);
        }
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            PrivacyMode.getInstance().changeMode(PrivacyMode.ACCESS_RESTRICTED);
            //Toast.makeText(getApplicationContext(), "Switch to private mode", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onKeyLongPress(keyCode, event);
    }

}